<?php
  class Redirect {
    public static function change($url) {
      header("Location: " . $url, true, 301);
      exit();
    }
  }
?>
