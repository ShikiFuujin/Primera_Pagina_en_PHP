<?php

  // BASE DE DATOS
  define("HOST", 'localhost');
  define("USERNAME", 'root');
  define("PASSWORD", '2405');
  define("PORT", '3306');
  define("BDNAME", 'blog');

  // RUTAS
  define("SERVER_PROTOCOLO", "http://");
  define("SERVER_IP", "localhost");
  define("SERVER_CONTEXTO", "/primer_php");
  define("SERVER_URL", SERVER_PROTOCOLO . SERVER_IP . SERVER_CONTEXTO);
  define("RUTA_REGISTRO", SERVER_URL . "/register.php");
  define("RUTA_REGISTRO_SUCCESS", SERVER_URL . "/register-success.php");
  define("RUTA_LOGIN", SERVER_URL . "/login.php");
?>
