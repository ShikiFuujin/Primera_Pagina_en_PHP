<?php
  include_once "controller.inc.php";
  include_once "usuario.inc.php";

  class UsuarioController extends Controller{
    public static function index() {
      $list = array();

      try {
        Connection::open();

        $conn = Connection::getConn();
        $sql = "SELECT * FROM usuario";
        $sentencia = $conn -> prepare($sql);
        $sentencia -> execute();
        $resultado = $sentencia -> fetchAll();

        if(count($resultado)){
          foreach ($resultado as $fila) {
            $list[] = new Usuario(
              $fila["id"]
              , $fila["username"]
              , ''
              , $fila["active"]
            );
          }
        } else {
          print "No hay resultado <br>";
        }

      } catch (PDOException $e) {
        print "ERROR : " . $e -> getMessage() . "<br>";
      }

      Connection::close();
      return $list;
    }

    public static function findByUsername(String $username) {
      $isPersist = false;

      try {
        Connection::open();

        $conn = Connection::getConn();
        $sql = "SELECT * FROM usuario WHERE username = :username";
        $sentencia = $conn -> prepare($sql);
        $sentencia -> bindParam(":username", $username);
        $sentencia -> execute();
        $resultado = $sentencia -> fetchAll();

        $isPersist = count($resultado) ? true : false;

      } catch (PDOException $e) {
        print "ERROR : " . $e -> getMessage() . "<br>";
      }

      Connection::close();
      return $isPersist;
    }

    public static function save(Usuario $usuario) {
      $isPersist = false;

      try {
        Connection::open();

        $conn = Connection::getConn();
        $sql = "INSERT INTO usuario (username, password, active) VALUES (:username, :password, 1)";
        $sentencia = $conn -> prepare($sql);

        $username = $usuario -> getUsername();
        $password = $usuario -> getPassword();

        $sentencia -> bindParam(":username", $username);
        $sentencia -> bindParam(":password", $password);
        $isPersist = $sentencia -> execute();

      } catch (PDOException $e) {
        print "ERROR : " . $e -> getMessage() . "<br>";
      }

      Connection::close();
      return $isPersist;
    }
  }
?>
