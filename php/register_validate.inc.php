<?php
  class RegisterValidate {
    private $username;
    private $password;

    public $errorUsername;
    public $errorPassword;
    public $errorPasswordRepeat;

    private $avisoInicio;
    private $avisoFin;

    private $validForm;

    // CONSTRUCTOR
    public function __construct($username, $password, $passwordRepeat){
      $avisoInicio = "<br><div class='alert alert-default' role='alert'>";
      $avisoFin = "</div>";

      $this -> username = "";
      $this -> password = "";
      $this -> errorUsername = $this -> validUsername($username);
      $this -> errorPassword = $this -> validPassword($password);
      $this -> errorPasswordRepeat = $this -> validPasswordRepeat($password, $passwordRepeat);

      $this -> validForm = $this -> errorUsername === ""
        && $this -> errorPassword === ""
        && $this -> errorPasswordRepeat === "";
    }

    // GETTERS
    public function getUsername() {
      return $this -> username;
    }
    public function getPassword() {
      return $this -> password;
    }
    public function getErrorUsername() {
      return $this -> errorUsername;
    }
    public function getErrorPassword() {
      return $this -> errorPassword;
    }
    public function getErrorPasswordRepeat() {
      return $this -> errorPasswordRepeat;
    }
    public function isValid() {
      return $this -> validForm;
    }

    // VALIDADORES
    private function validSet($var) {
      return isset($var) && !empty($var);
    }

    private function validUsername($username) {
      if(!$this -> validSet($username)) {
        return "Debes escribir el username";
      }else {
        $this -> username = $username;
        if(!preg_match("/^([a-zA-Z0-9]){6,24}$/", $username)) {
          return "Debe cumplir el formato de Nombre de usuario, de 6 a 24 caracteres alfanumericos";
        } else if(UsuarioController::findByUsername($username)) {
          return "El nombre de usuario ya está registrado.";
        } else {
          return "";
        }
      }
    }

    private function validPassword($password) {
      if(!$this -> validSet($password)) {
        return "Debes escribir la contraseña";
      } else {
        $this -> password = $password;
        if(!preg_match("/^([a-zA-Z0-9]){4,8}$/", $password)) {
          return "Debe cumplir el formato de Contraseña, de 4 a 8 caracteres alfanumericos";
        } else {
          return "";
        }
      }
    }

    private function validPasswordRepeat($password, $passwordRepeat) {
      if(!$this -> validSet($password)) {
        return "La contraseña no ha sido escrita";
      } else if($password !== $passwordRepeat) {
        return "La repetición de la contraseña debe ser igual";
      } else {
        return "";
      }
    }

    // VALUES
    public function showUsername() {
      if($this -> username !== "") {
        echo 'value="' . $this -> username . '"';
      }
    }
    public function showErrorUsername() {
      if($this -> errorUsername !== "") {
        echo $this -> avisoInicio . $this -> errorUsername . $this -> avisoFin;
      }
    }
    public function showErrorPassword() {
      if($this -> errorPassword !== "") {
        echo $this -> avisoInicio . $this -> errorPassword . $this -> avisoFin;
      }
    }
    public function showErrorPasswordRepeat() {
      if($this -> errorPasswordRepeat !== "") {
        echo $this -> avisoInicio . $this -> errorPasswordRepeat . $this -> avisoFin;
      }
    }
  }
?>
