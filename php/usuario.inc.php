<?php
  class Usuario {
    private $id;
    private $username;
    private $password;
    private $active;

    // CONTRUCTOR
    public function __construct($id, $username, $password, $active) {
      $this -> id = $id;
      $this -> username = $username;
      $this -> password = $password;
      $this -> active = $active;
    }

    // GETTERs
    public function getId() {
      return $this -> id;
    }
    public function getUsername() {
      return $this -> username;
    }
    public function getPassword() {
      return $this -> password;
    }
    public function isActive() {
      return $this -> active;
    }

    // SETTERs
    public function setId($id) {
      return $this -> id = $id;
    }
    public function setUsername($username) {
      return $this -> username = $username;
    }
    public function setPassword($password) {
      return $this -> password = $password;
    }
    public function setActive($active) {
      return $this -> active = $active;
    }
  }
?>
