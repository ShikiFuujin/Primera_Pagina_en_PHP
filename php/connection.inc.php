<?php
  class Connection {
    private static $conn;

    // ABRIR CONEXIÓN
    public static function open() {
      if(!isset(self::$conn)){
        try {
          include_once "php/config.inc.php";
          self::$conn = new PDO('mysql:host=' . HOST . '; port=' . PORT . '; dbname=' . BDNAME
            , USERNAME
            , PASSWORD);
          self::$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          self::$conn -> exec("SET CHARACTER SET utf8");
        } catch (PDOException $e) {
          print "ERROR : " . $e -> getMessage() . "<br>";
          die();
        }
      }
    }

    // CERRAR CONEXIÓN
    public static function close() {
      if(isset(self::$conn)){
        self::$conn = null;
      }
    }

    public static function getConn() {
      return self::$conn;
    }
  }
 ?>
