<?php
  $pageTitle = "Registro Exitoso";
  include_once 'template/inicio.inc.php';

  if(isset($_GET["username"]) && !empty($_GET["username"])) {
    $username = $_GET["username"];
  } else {
    Redirect::change(SERVER_URL);
  }
?>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h2><i>Gracias</i> por registrarse <b><?php echo $username; ?></b></h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <br>
      <a href="<?php echo RUTA_LOGIN; ?>">Inicia Sesión</a>
    </div>
  </div>
</div>

<?php include_once 'template/fin.inc.php'; ?>
