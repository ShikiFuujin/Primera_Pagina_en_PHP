<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <?php
    if(!isset($pageTitle) || empty($pageTitle)) {
      $pageTitle = "titulo por defecto";
    }
    echo "<title>$pageTitle</title>";
  ?>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="index.css">
</head>
<body>
  <?php
    include_once "php/config.inc.php";
    include_once 'php/redirect.inc.php';
    include_once 'content/nav.inc.php';
  ?>
