<fieldset class="form-group">
  <label for="">Nombre de Usuario</label>
  <input class="form-control" name="username" type="text" placeholder="user_name" required <?php if(isset($_POST["persist"])){ $validate -> showUsername(); }?>>
  <?php if(isset($_POST["persist"])){ $validate -> showErrorUsername(); } ?>
</fieldset>
<fieldset class="form-group">
  <label for="">Contraseña</label>
  <input class="form-control" name="password" type="password" placeholder="****" required>
  <?php if(isset($_POST["persist"])){ $validate -> showErrorPassword(); } ?>
</fieldset>
<fieldset class="form-group">
  <label for="">Repita Contraseña</label>
  <input class="form-control" name="password_repeat" type="password" placeholder="****" required>
  <?php if(isset($_POST["persist"])){ $validate -> showErrorPasswordRepeat(); } ?>
</fieldset>
<input type="reset" class="btn btn-default btn-secondary" value="Limpiar">
<input type="submit" class="btn btn-default btn-primary" name="persist" value="Guardar">
