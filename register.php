<?php
    if(isset($_POST["persist"])){
    include_once 'php/usuarioController.inc.php';
    $validate = new RegisterValidate(
      $_POST["username"]
      , $_POST["password"]
      , $_POST["password_repeat"]
    );

    if($validate -> isValid()) {
      $usuario = new Usuario(null
        , $validate -> getUsername()
        , password_hash($validate -> getPassword(), PASSWORD_DEFAULT)
        , true);
      $isPersist = UsuarioController::save($usuario);
      if($isPersist) {
        include_once 'php/redirect.inc.php';
        Redirect::change(RUTA_REGISTRO_SUCCESS . "?username=" . $validate -> getUsername());
      }
    }
  }

  $pageTitle = "Registro";
  include_once 'template/inicio.inc.php';
?>

<div class="container">
  <div class="row">
    <div class="col-md-7">
      <article class="text-center">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos explicabo nisi dicta nesciunt voluptatem necessitatibus ullam accusantium maxime et porro voluptate nam consequuntur accusamus labore, eligendi facilis perspiciatis numquam ipsam.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </article>
    </div>
    <div class="col-md-5">
      <h3 class="">Registro de datos</h3>
      <div class="panel-body">
        <form method="post" role="form" action="<?php echo $_SERVER['PHP_SELF'] ?>">
          <?php
            include_once 'template/register/form.inc.php';
          ?>
        </form>
      </div>
    </div>
  </div>
</div>

<?php
  include_once 'template/fin.inc.php';
?>
